const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let date = new Date().valueOf();

// JavaScript
mix
    .js('resources/js/bootstrap.js', 'public/js/app.js')
    .webpackConfig({
        output: {
            chunkFilename: 'js/chunks/[name].js?id=' + date,
        },
    });

// SASS
mix
    .sass('resources/sass/bootstrap.scss', 'public/css/app.css?id=' + date)
    .options({
        postCss: [
            tailwindcss('tailwind.config.js')
        ],
        processCssUrls: false,
    });
