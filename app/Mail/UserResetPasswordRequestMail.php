<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserResetPasswordRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public string $link;

    public function __construct(User $user, string $link)
    {
        $this->user = $user;

        $this->link = $link;
    }

    public function build(): Mailable
    {
        $data = [
            'link' => $this->link,
            'user' => $this->user,
        ];

        return $this->subject('Forgot Password')->markdown('mail.user-reset-password-request')->with($data);
    }
}
