<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build(): Mailable
    {
        $data = [
            'user' => $this->user,
        ];

        return $this->subject('Password Reset')->markdown('mail.user-reset-password')->with($data);
    }
}
