<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;

    public string $link;

    public function __construct(User $user, string $link)
    {
        $this->user = $user;

        $this->link = $link;
    }

    public function build(): Mailable
    {
        $data = [
            'user' => $this->user,
            'link' => $this->link,
        ];

        return $this->subject('Welcome')->markdown('mail.user-welcome')->with($data);
    }
}
