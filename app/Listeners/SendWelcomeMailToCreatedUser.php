<?php

namespace App\Listeners;

use App\Events\UserCreatedEvent;
use App\Mail\UserCreatedMail;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeMailToCreatedUser implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(UserCreatedEvent $event): void
    {
        Mail::to($event->user)->send(new UserCreatedMail($event->user, $event->link));
    }

    public function failed(UserCreatedEvent $event, Exception $exception): void
    {
        //
    }
}
