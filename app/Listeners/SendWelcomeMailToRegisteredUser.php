<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use App\Mail\UserRegisteredMail;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeMailToRegisteredUser implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(UserRegisteredEvent $event): void
    {
        Mail::to($event->user)->send(new UserRegisteredMail($event->user, $event->link));
    }

    public function failed(UserRegisteredEvent $event, Exception $exception): void
    {
        //
    }
}
