<?php

namespace App\Listeners;

use App\Events\UserResetPasswordRequestEvent;
use App\Mail\UserResetPasswordRequestMail;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendResetPasswordRequestMailToUser implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(UserResetPasswordRequestEvent $event): void
    {
        Mail::to($event->user)->send(new UserResetPasswordRequestMail($event->user, $event->link));
    }

    public function failed(UserResetPasswordRequestEvent $event, Exception $exception): void
    {
        //
    }
}
