<?php

namespace App\Listeners;

use App\Events\UserResetPasswordEvent;
use App\Mail\UserResetPasswordMail;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendResetPasswordMailToUser implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(UserResetPasswordEvent $event): void
    {
        Mail::to($event->user)->send(new UserResetPasswordMail($event->user));
    }

    public function failed(UserResetPasswordEvent $event, Exception $exception): void
    {
        //
    }
}
