<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\UserType
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType whereName($value)
 * @mixin \Eloquent
 */
class UserType extends Model
{
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
