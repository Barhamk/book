<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserResetPasswordRequestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public User $user;

    public string $link;

    public function __construct(User $user, string $link)
    {
        $this->user = $user;

        $this->link = $link;
    }
}
