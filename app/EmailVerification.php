<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\EmailVerification
 *
 * @property string $email
 * @property string $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\EmailVerification whereToken($value)
 * @mixin \Eloquent
 */
class EmailVerification extends Model
{
    protected $dates = [
        'created_at',
    ];

    protected $primaryKey = 'email';

    public $incrementing = false;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
