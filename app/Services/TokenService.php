<?php

namespace App\Services;

use Illuminate\Support\Str;

class TokenService
{
    public function generateUnique(): string
    {
        return Str::random(60);
    }
}
