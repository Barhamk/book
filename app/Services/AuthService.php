<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login(string $email, string $password): ?string
    {
        return auth()->attempt(['email' => $email, 'password' => $password]) ?: null;
    }

    public function loginWithUser(User $user): string
    {
        return auth()->login($user);
    }

    public function logout(): bool
    {
        auth()->logout();

        return true;
    }

    public function me(): User
    {
        return Auth::user();
    }

    public function refresh(): string
    {
        return auth()->refresh();
    }
}
