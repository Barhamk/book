<?php

namespace App\Services;

use App\EmailVerification;
use App\Repositories\EmailVerificationRepository;
use App\Repositories\UserRepository;
use App\User;
use Carbon\Carbon;

class EmailVerificationService
{
    private EmailVerificationRepository $emailVerificationRepository;

    private TokenService $tokenService;

    private UserRepository $userRepository;

    public function __construct(
        EmailVerificationRepository $emailVerificationRepository,
        TokenService $tokenService,
        UserRepository $userRepository
    ) {
        $this->emailVerificationRepository = $emailVerificationRepository;

        $this->tokenService = $tokenService;

        $this->userRepository = $userRepository;
    }

    public function checkEmailVerificationTokenMatches(User $user, string $token): bool
    {
        $emailVerification = $this->emailVerificationRepository->findFirstWhere('token', $token);

        if (null === $emailVerification) {
            return false;
        }

        if ($emailVerification->email !== $user->email) {
            return false;
        }

        return true;
    }

    public function createNewEmailVerification(User $user): EmailVerification
    {
        $emailVerification = new EmailVerification;
        $emailVerification->token = $this->tokenService->generateUnique();
        $emailVerification->created_at = Carbon::now(config('app.timezone'));
        $emailVerification->user()->associate($user);

        $this->emailVerificationRepository->save($emailVerification);

        return $emailVerification;
    }

    public function generateEmailVerificationLink(User $user): string
    {
        return config('app.url') . '?modal=verify-email&email=' . $user->email . '&token=' . $user->emailVerification->token;
    }

    public function verifyEmail(User $user): User
    {
        $emailVerification = $user->emailVerification;

        $this->emailVerificationRepository->delete($emailVerification);

        $user->email_verified_at = Carbon::now();

        $this->userRepository->save($user);

        return $this->userRepository->refresh($user);
    }
}
