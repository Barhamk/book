<?php

namespace App\Services;

use App\Profile;
use App\Repositories\ProfileRepository;
use App\User;

class ProfileService
{
    private ProfileRepository $profileRepository;

    public function __construct(
        ProfileRepository $profileRepository
    ) {
        $this->profileRepository = $profileRepository;
    }

    public function createAdministrator(User $user, array $data): Profile
    {
        $profile = new Profile;
        $profile->name = $data['name'];
        $profile->user()->associate($user);

        $this->profileRepository->save($profile);

        return $this->profileRepository->refresh($profile);
    }

    public function createCustomer(User $user, array $data): Profile
    {
        $profile = new Profile;
        $profile->name = $data['name'];
        $profile->user()->associate($user);

        $this->profileRepository->save($profile);

        return $this->profileRepository->refresh($profile);
    }

    public function updateAdministrator(Profile $profile, array $data): Profile
    {
        $profile->name = $data['name'];

        $this->profileRepository->save($profile);

        return $profile;
    }

    public function updateCustomer(Profile $profile, array $data): Profile
    {
        $profile->name = $data['name'];

        $this->profileRepository->save($profile);

        return $profile;
    }
}
