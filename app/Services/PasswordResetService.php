<?php

namespace App\Services;

use App\PasswordReset;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class PasswordResetService
{
    private PasswordResetRepository $passwordResetRepository;

    private TokenService $tokenService;

    private UserRepository $userRepository;

    public function __construct(
        PasswordResetRepository $passwordResetRepository,
        TokenService $tokenService,
        UserRepository $userRepository
    ) {
        $this->passwordResetRepository = $passwordResetRepository;

        $this->tokenService = $tokenService;

        $this->userRepository = $userRepository;
    }

    public function checkPasswordResetTokenMatches(User $user, string $token): bool
    {
        $passwordReset = $this->passwordResetRepository->findFirstWhere('token', $token);

        if (null === $passwordReset) {
            return false;
        }

        if ($passwordReset->email !== $user->email) {
            return false;
        }

        if ($passwordReset->created_at->diffInHours(Carbon::now(config('app.timezone'))) >= 1) {
            return false;
        }

        return true;
    }

    public function createNewPasswordReset(User $user): PasswordReset
    {
        $passwordReset = $user->passwordReset;

        if ($passwordReset) {
            $this->passwordResetRepository->delete($passwordReset);
        }

        $passwordReset = new PasswordReset;
        $passwordReset->token = $this->tokenService->generateUnique();
        $passwordReset->created_at = Carbon::now(config('app.timezone'));
        $passwordReset->user()->associate($user);

        $this->passwordResetRepository->save($passwordReset);

        return $passwordReset;
    }

    public function generatePasswordResetLink(User $user): string
    {
        return config('app.url') . '?modal=reset-password&token=' . $user->passwordReset->token;
    }

    public function saveNewPassword(User $user, string $password): User
    {
        $passwordReset = $user->passwordReset;

        $this->passwordResetRepository->delete($passwordReset);

        $user->password = Hash::make($password);

        $this->userRepository->save($user);

        return $this->userRepository->refresh($user);
    }
}
