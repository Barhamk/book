<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\UserTypeRepository;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    private UserRepository $userRepository;

    private UserTypeRepository $userTypeRepository;

    public function __construct(
        UserRepository $userRepository,
        UserTypeRepository $userTypeRepository
    ) {
        $this->userRepository = $userRepository;

        $this->userTypeRepository = $userTypeRepository;
    }

    public function create(array $data, string $type): User
    {
        $userType = $this->userTypeRepository->findFirstWhere('name', $type);

        $user = new User;
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->userType()->associate($userType);

        $this->userRepository->save($user);

        return $this->userRepository->refresh($user);
    }

    public function update(User $user, array $data): User
    {
        $user->email = $data['email'];

        $this->userRepository->save($user);

        return $user;
    }
}
