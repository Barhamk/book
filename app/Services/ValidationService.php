<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Validator;

class ValidationService
{
    protected Validator $validator;

    public function login(array $data): array
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users',
            'password' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function register(array $data): array
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|same:passwordConfirmation',
            'passwordConfirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function passwordReset(array $data): array
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users',
            'password' => 'required|string|min:8|same:passwordConfirmation',
            'passwordConfirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function passwordResetRequest(array $data): array
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function emailVerify(array $data): array
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users',
            'token' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function userCreate(array $data): array
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|same:passwordConfirmation',
            'passwordConfirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }

    public function userUpdate(array $data, User $user): array
    {
        $rules = [
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }
}
