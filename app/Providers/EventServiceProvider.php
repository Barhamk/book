<?php

namespace App\Providers;

use App\Events\UserCreatedEvent;
use App\Events\UserDeletedEvent;
use App\Events\UserRegisteredEvent;
use App\Events\UserResetPasswordEvent;
use App\Events\UserResetPasswordRequestEvent;
use App\Listeners\SendGoodbyeMailToUser;
use App\Listeners\SendResetPasswordMailToUser;
use App\Listeners\SendResetPasswordRequestMailToUser;
use App\Listeners\SendWelcomeMailToCreatedUser;
use App\Listeners\SendWelcomeMailToRegisteredUser;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserCreatedEvent::class => [
            SendWelcomeMailToCreatedUser::class,
        ],
        UserRegisteredEvent::class => [
            SendWelcomeMailToRegisteredUser::class,
        ],
        UserResetPasswordEvent::class => [
            SendResetPasswordMailToUser::class,
        ],
        UserResetPasswordRequestEvent::class => [
            SendResetPasswordRequestMailToUser::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
