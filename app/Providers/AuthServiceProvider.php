<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-user', 'App\Policies\UserPolicy@create');

        Gate::define('delete-user', 'App\Policies\UserPolicy@delete');

        Gate::define('index-users', 'App\Policies\UserPolicy@index');

        Gate::define('update-user', 'App\Policies\UserPolicy@update');

        Gate::define('view-user', 'App\Policies\UserPolicy@view');
    }
}
