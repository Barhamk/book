<?php

namespace App\Repositories;

use App\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class UserRepository
{
    public function all(): Collection
    {
        return User::all();
    }

    public function allAdministrators(): Collection
    {
        return User::administrators()->get();
    }

    public function allCustomers(): Collection
    {
        return User::customers()->get();
    }

    public function allPaginated(int $perPage): LengthAwarePaginator
    {
        return User::paginate($perPage);
    }

    public function allPaginatedAdministrators(int $perPage): LengthAwarePaginator
    {
        return User::administrators()->paginate($perPage);
    }

    public function allPaginatedCustomers(int $perPage): LengthAwarePaginator
    {
        return User::customers()->paginate($perPage);
    }

    public function delete(User $user): bool
    {
        try {
            $user->delete();

            return true;
        } catch (Exception $exception) {
            report($exception);

            return false;
        }
    }

    public function find(int $id): ?User
    {
        try {
            return User::findOrFail($id);
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }

    public function findFirstWhere(string $column, $value): ?User
    {
        try {
            return User::where($column, $value)->firstOrFail();
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }

    public function refresh(User $user): User
    {
        return $user->refresh();
    }

    public function save(User $user): bool
    {
        try {
            $user->saveOrFail();

            return true;
        } catch (Throwable $exception) {
            report($exception);

            return false;
        }
    }
}
