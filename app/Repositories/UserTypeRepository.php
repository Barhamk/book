<?php

namespace App\Repositories;

use App\UserType;

class UserTypeRepository
{
    public function findFirstWhere(string $column, $value): ?UserType
    {
        try {
            return UserType::where($column, $value)->firstOrFail();
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }
}
