<?php

namespace App\Repositories;

use App\Profile;
use Exception;
use Throwable;

class ProfileRepository
{
    public function findFirstWhere(string $column, $value): ?Profile
    {
        try {
            return Profile::where($column, $value)->firstOrFail();
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }

    public function refresh(Profile $profileCustomer): Profile
    {
        return $profileCustomer->refresh();
    }

    public function save(Profile $profileCustomer): bool
    {
        try {
            $profileCustomer->saveOrFail();

            return true;
        } catch (Throwable $exception) {
            report($exception);

            return false;
        }
    }
}
