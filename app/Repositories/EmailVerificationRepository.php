<?php

namespace App\Repositories;

use App\EmailVerification;
use Exception;
use Throwable;

class EmailVerificationRepository
{
    public function delete(EmailVerification $emailVerification): bool
    {
        try {
            $emailVerification->delete();

            return true;
        } catch (Exception $exception) {
            report($exception);

            return false;
        }
    }

    public function findFirstWhere(string $column, $value): ?EmailVerification
    {
        try {
            return EmailVerification::where($column, $value)->firstOrFail();
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }

    public function refresh(EmailVerification $emailVerification): EmailVerification
    {
        return $emailVerification->refresh();
    }

    public function save(EmailVerification $emailVerification): bool
    {
        try {
            $emailVerification->saveOrFail();

            return true;
        } catch (Throwable $exception) {
            report($exception);

            return false;
        }
    }
}
