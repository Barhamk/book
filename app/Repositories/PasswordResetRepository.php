<?php

namespace App\Repositories;

use App\PasswordReset;
use Exception;
use Throwable;

class PasswordResetRepository
{
    public function delete(PasswordReset $passwordReset): bool
    {
        try {
            $passwordReset->delete();

            return true;
        } catch (Exception $exception) {
            report($exception);

            return false;
        }
    }

    public function findFirstWhere(string $column, $value): ?PasswordReset
    {
        try {
            return PasswordReset::where($column, $value)->firstOrFail();
        } catch (Exception $exception) {
            report($exception);

            return null;
        }
    }

    public function refresh(PasswordReset $passwordReset): PasswordReset
    {
        return $passwordReset->refresh();
    }

    public function save(PasswordReset $passwordReset): bool
    {
        try {
            $passwordReset->saveOrFail();

            return true;
        } catch (Throwable $exception) {
            report($exception);

            return false;
        }
    }
}
