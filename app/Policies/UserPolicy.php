<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $auth, string $ability)
    {
        return $auth->isAdministrator()?: null;
    }

    public function create(User $auth): bool
    {
        return false;
    }

    public function delete(User $auth, User $user): bool
    {
        return $auth->is($user);
    }

    public function index(User $auth): bool
    {
        return false;
    }

    public function update(User $auth, User $user): bool
    {
        return $auth->is($user);
    }

    public function view(User $auth, User $user): bool
    {
        return $auth->is($user);
    }
}
