<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function response200($data, array $headers = []): JsonResponse
    {
        $response = [
            'body' => $data,
            'status' => 200,
        ];

        return response()->json($response, 200)->withHeaders($headers);
    }

    protected function response400($data, array $headers = []): JsonResponse
    {
        $response = [
            'body' => $data,
            'status' => 400,
        ];

        return response()->json($response, 400)->withHeaders($headers);
    }

    protected function response401($data = 'Unauthorised', array $headers = []): JsonResponse
    {
        $response = [
            'body' => $data,
            'status' => 401,
        ];

        return response()->json($response, 401)->withHeaders($headers);
    }

    protected function response422($data, array $headers = []): JsonResponse
    {
        $response = [
            'body' => $data,
            'status' => 422,
        ];

        return response()->json($response, 422)->withHeaders($headers);
    }

    protected function response500($data = 'Failure', array $headers = []): JsonResponse
    {
        $response = [
            'body' => $data,
            'status' => 500,
        ];

        return response()->json($response, 500)->withHeaders($headers);
    }
}
