<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegisteredEvent;
use App\Events\UserResetPasswordEvent;
use App\Events\UserResetPasswordRequestEvent;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\AuthService;
use App\Services\EmailVerificationService;
use App\Services\PasswordResetService;
use App\Services\ProfileService;
use App\Services\UserService;
use App\Services\ValidationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected AuthService $authService;

    protected EmailVerificationService $emailVerificationService;

    protected PasswordResetService $passwordResetService;

    protected ProfileService $profileService;

    protected UserRepository $userRepository;

    protected UserService $userService;

    protected ValidationService $validationService;

    public function __construct(
        AuthService $authService,
        EmailVerificationService $emailVerificationService,
        PasswordResetService $passwordResetService,
        ProfileService $profileService,
        UserRepository $userRepository,
        UserService $userService,
        ValidationService $validationService
    ) {
        $this->authService = $authService;

        $this->emailVerificationService = $emailVerificationService;

        $this->passwordResetService = $passwordResetService;

        $this->profileService = $profileService;

        $this->userRepository = $userRepository;

        $this->userService = $userService;

        $this->validationService = $validationService;
    }

    public function emailVerify(Request $request): JsonResponse
    {
        $data = $request->all();

        $errors = $this->validationService->emailVerify($data);

        if ($errors) {
            return $this->response422($errors);
        }

        $user = $this->userRepository->findFirstWhere('email', $data['email']);

        if (null === $user) {
            return $this->response400('User not found');
        }

        $check = $this->emailVerificationService->checkEmailVerificationTokenMatches($user, $data['token']);

        if (false === $check) {
            return $this->response422('Token Mismatch');
        }

        $this->emailVerificationService->verifyEmail($user);

        return $this->response200('Success');
    }

    public function login(Request $request): JsonResponse
    {
        $data = $request->all();

        $errors = $this->validationService->login($data);

        if ($errors) {
            return $this->response422($errors);
        }

        $token = $this->authService->login($data['email'], $data['password']);

        if (null === $token) {
            return $this->response422(['password' => ['Wrong password. Please try again.']]);
        }

        return $this->response200($token, ['x-auth-token' => $token]);
    }

    public function logout(): JsonResponse
    {
        $successful = $this->authService->logout();

        if (false === $successful) {
            return $this->response500();
        }

        return $this->response200('Success');
    }

    public function me(): JsonResponse
    {
        return $this->response200($this->authService->me());
    }

    public function passwordResetRequest(Request $request): JsonResponse
    {
        $data = $request->all();

        $errors = $this->validationService->passwordResetRequest($data);

        if ($errors) {
            return $this->response422($errors);
        }

        $user = $this->userRepository->findFirstWhere('email', $data['email']);

        if (null === $user) {
            return $this->response400('User not found');
        }

        $this->passwordResetService->createNewPasswordReset($user);

        $user = $this->userRepository->refresh($user);

        $link = $this->passwordResetService->generatePasswordResetLink($user);

        event(new UserResetPasswordRequestEvent($user, $link));

        return $this->response200('Success');
    }

    public function passwordReset(Request $request): JsonResponse
    {
        $data = $request->all();

        $errors = $this->validationService->passwordReset($data);

        if ($errors) {
            return $this->response422($errors);
        }

        if (false === isset($data['token'])) {
            return $this->response422(['email' => ['Please request a new password reset.']]);
        }

        $user = $this->userRepository->findFirstWhere('email', $data['email']);

        if (null === $user) {
            return $this->response400('User not found');
        }

        $check = $this->passwordResetService->checkPasswordResetTokenMatches($user, $data['token']);

        if (false === $check) {
            return $this->response422(['email' => ['Please request a new password reset.']]);
        }

        $user = $this->passwordResetService->saveNewPassword($user, $data['password']);

        event(new UserResetPasswordEvent($user));

        return $this->response200('Success');
    }

    public function refresh(): JsonResponse
    {
        $token = $this->authService->refresh();

        return $this->response200($token, ['x-auth-token' => $token]);
    }

    public function register(Request $request): JsonResponse
    {
        $data = $request->all();

        $errors = $this->validationService->register($data);

        if ($errors) {
            return $this->response422($errors);
        }

        $user = $this->userService->create($data, 'customer');

        $this->profileService->createCustomer($user, $data);

        $this->emailVerificationService->createNewEmailVerification($user);

        $user = $this->userRepository->refresh($user);

        $token = $this->authService->loginWithUser($user);

        $link = $this->emailVerificationService->generateEmailVerificationLink($user);

        event(new UserRegisteredEvent($user, $link));

        return $this->response200($token, ['x-auth-token' => $token]);
    }
}
