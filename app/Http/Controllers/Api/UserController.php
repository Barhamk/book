<?php

namespace App\Http\Controllers\Api;

use App\Events\UserCreatedEvent;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\AuthService;
use App\Services\EmailVerificationService;
use App\Services\ProfileService;
use App\Services\UserService;
use App\Services\ValidationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public AuthService $authService;

    public EmailVerificationService $emailVerificationService;

    public UserRepository $userRepository;

    public ProfileService $profileService;

    public UserService $userService;

    protected ValidationService $validationService;

    public function __construct(
        AuthService $authService,
        EmailVerificationService $emailVerificationService,
        ProfileService $profileService,
        UserRepository $userRepository,
        UserService $userService,
        ValidationService $validationService
    ) {
        $this->authService = $authService;

        $this->emailVerificationService = $emailVerificationService;

        $this->profileService = $profileService;

        $this->userRepository = $userRepository;

        $this->userService = $userService;

        $this->validationService = $validationService;
    }

    public function destroy(int $id): JsonResponse
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            return $this->response400('User not found');
        }

        if (Gate::denies('delete-user', $user)) {
            return $this->response401();
        }

        $this->userRepository->delete($user);

        return $this->response200('Success');
    }

    public function index(Request $request): JsonResponse
    {
        if (Gate::denies('index-users')) {
            return $this->response401();
        }

        $perPage = $request->query('perPage', 10);

        return $this->response200($this->userRepository->allPaginated($perPage));
    }

    public function show(int $id): JsonResponse
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            return $this->response400('User not found');
        }

        if (Gate::denies('view-user', $user)) {
            return $this->response401();
        }

        return $this->response200($user);
    }

    public function store(Request $request): JsonResponse
    {
        if (Gate::denies('create-user')) {
            return $this->response401();
        }

        $data = $request->all();

        $errors = $this->validationService->userCreate($data);

        if ($errors) {
            return $this->response422($errors);
        }

        $user = $this->userService->create($data, 'customer');

        $this->profileService->createCustomer($user, $data);

        $this->emailVerificationService->createNewEmailVerification($user);

        $user = $this->userRepository->refresh($user);

        $link = $this->emailVerificationService->generateEmailVerificationLink($user);

        event(new UserCreatedEvent($user, $link));

        return $this->response200($user);
    }

    public function update(Request $request, int $id): JsonResponse
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            return $this->response400('User not found');
        }

        if (Gate::denies('update-user', $user)) {
            return $this->response401();
        }

        $data = $request->all();

        $errors = $this->validationService->userUpdate($data, $user);

        if ($errors) {
            return $this->response422($errors);
        }

        $user = $this->userService->update($user, $data);

        return $this->response200($user);
    }
}
