<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>BASE</title>

        <meta charset="utf-8">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/assets/favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/assets/favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('/assets/favicon/site.webmanifest') }}">

        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    </head>

    <body>
        <div id="app" class="app">
            <vue-progress-bar></vue-progress-bar>

            <modal></modal>

            <pane-t></pane-t>

            <pane-r></pane-r>

            <pane-l></pane-l>

            <main>
                <router-view></router-view>
            </main>
        </div>

        <script src="{{ asset('/js/app.js') }}"></script>
    </body>
</html>
