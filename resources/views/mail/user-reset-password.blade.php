@component('mail::message')
# Password Reset

Your password is successfully reset. If you didn't request this, please contact our support team.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
