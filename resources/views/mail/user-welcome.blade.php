@component('mail::message')
# Welcome

Thank you for registering with us. Please use the link below to verify your account.

@component('mail::button', ['url' => $link])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
