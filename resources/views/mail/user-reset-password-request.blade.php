@component('mail::message')
# Reset Password

Please use the link below to reset your password. This link will expire in one hour. If the time expires, please make a new password reset request.

@component('mail::button', ['url' => $link])
Reset Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
