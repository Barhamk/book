import Axios from 'axios';
import Storage from '../services/Storage';

export default {

    namespaced: true,

    state: {
        authenticated: false,
        me: null,
        token: null,
    },

    getters: {
        getAuthenticated: (state, getters) => (data = {}) => {
            return state.authenticated;
        },

        getMe: (state, getters) => (data = {}) => {
            return state.me;
        },

        getToken: (state, getters) => (data = {}) => {
            return state.token;
        },
    },

    actions: {
        doLogin({commit, state}, data = {}) {
            return Axios.post('/auth/login', data).then((response) => {
                commit('setAuthenticated', true);

                return response.data.body;
            });
        },

        doLogout({commit, state}, data = {}) {
            return Axios.post('/auth/logout').then((response) => {
                Storage.clear();

                delete Axios.defaults.headers.common.Authorization;

                window.location.href = '/';

                return response.data.body;
            }).catch((error) => {
                Storage.clear();

                delete Axios.defaults.headers.common.Authorization;

                window.location.href = '/';

                return error;
            });
        },

        doRefresh({commit, state}, data = {}) {
            return Axios.post('/auth/refresh', data).then((response) => {
                return response.data.body;
            });
        },

        doRegister({commit, state}, data = {}) {
            return Axios.post('/auth/register', data).then((response) => {
                commit('setAuthenticated', true);

                return response.data.body;
            });
        },

        doResetPassword({commit, state}, data = {}) {
            return Axios.post('/auth/password-reset', data).then((response) => {
                return response.data.body;
            });
        },

        doResetPasswordRequest({commit, state}, data = {}) {
            return Axios.post('/auth/password-reset-request', data).then((response) => {
                return response.data.body;
            });
        },

        doVerifyEmail({commit, state}, data = {}) {
            return Axios.post('/auth/email-verify', data).then((response) => {
                return response.data.body;
            });
        },

        fetchMe({commit, state}, data = {}) {
            return Axios.get('/auth/me').then((response) => {
                commit('setMe', response.data.body);

                return response.data.body;
            });
        },
    },

    mutations: {
        setAuthenticated(state, data = null) {
            state.authenticated = data;
        },

        setMe(state, data = null) {
            state.me = data;
        },

        setToken(state, data = null) {
            state.token = data;
        },
    },

};
