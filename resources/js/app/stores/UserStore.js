import Axios from 'axios';

export default {

    namespaced: true,

    state: {
        user: null,
        users: null,
    },

    getters: {
        getUser: (state, getters) => (data = {}) => {
            return state.user;
        },

        setUsers: (state, getters) => (data = {}) => {
            return state.users;
        },
    },

    actions: {
        fetchUser({commit, state}, data = {}) {
            return Axios.get('/users/' + data)
                .then((response) => {
                    commit('setUser', response.data.body);

                    return response.data.body;
                });
        },

        fetchUsers({commit, state}, data = {}) {
            return Axios.get('/users', { params: data })
                .then((response) => {
                    commit('setUsers', response.data.body);

                    return response.data.body;
                });
        },
    },

    mutations: {
        setUser(state, data = null) {
            state.user = data;
        },

        setUsers(state, data = null) {
            state.users = data;
        },
    },

};
