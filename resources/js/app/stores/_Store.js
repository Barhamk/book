import Axios from 'axios';

export default {

    namespaced: true,

    state: {
        name: null,
    },

    getters: {
        getName: (state, getters) => (data = {}) => {
            return state.name;
        },
    },

    actions: {
        fetchName({commit, state}, data = {}) {
            let num = Math.floor(Math.random() * 10) + 1;

            return Axios.get('https://jsonplaceholder.typicode.com/users/' + num)
                .then((response) => {
                    commit('setName', response.data.name);

                    return response.data;
                });
        },
    },

    mutations: {
        setName(state, data = null) {
            state.name = data;
        },
    },

};
