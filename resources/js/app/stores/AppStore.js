export default {

    namespaced: true,

    state: {
        modal: false,
        modalShow: false,
        paneL: false,
        paneR: false,
    },

    getters: {
        getModalBody: (state, getters) => (data = {}) => {
            return state.modal;
        },

        getModalShow: (state, getters) => (data = {}) => {
            return state.modalShow;
        },

        getPaneL: (state, getters) => (data = {}) => {
            return state.paneL;
        },

        getPaneR: (state, getters) => (data = {}) => {
            return state.paneR;
        },
    },

    actions: {
        setModal({commit, state}, data) {
            if (data) commit('setModal', data);

            commit('setModalShow', !!data);

            return true;
        },
    },

    mutations: {
        setModal(state, data = false) {
            state.modal = data;
        },

        setModalShow(state, data = false) {
            state.modalShow = data;
        },

        setPaneL(state, data = false) {
            state.paneL = data;
        },

        setPaneR(state, data = false) {
            state.paneR = data;
        },
    },

};
