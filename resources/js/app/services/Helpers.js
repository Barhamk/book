import Log from './Log';

export default new class {

    hasProperties(fields, target) {
        if (fields.constructor === Object) fields = Object.keys(fields);

        if (target.constructor === Object) target = Object.keys(target);

        return _.isEqual(fields.sort(), target.sort());
    }

    isMobile() {
        return window.innerWidth < 1024;
    }

    loading() {
        document.title = this.title + ' - Loading';
    }

    loadingStop() {
        document.title = this.title + (this.titleSub ? ' - ' + this.titleSub : '');
    }

    metaTitle(to) {
        this.title = document.title.split(' - ')[0];

        if (typeof to === 'string') {
            this.titleSub = to ? to : null;
        } else {
            if (to) {
                this.titleSub = to.meta && to.meta.title ? to.meta && to.meta.title : null;

                if (!this.titleSub) Log.warning(`[Helpers] The route "${to.name}" does not have a title.`);
            } else {
                this.titleSub = null;
            }
        }

        document.title = this.title + (this.titleSub ? ' - ' + this.titleSub : '');
    }

    resetComponentData(component) {
        Object.assign(component.$data, component.$options.data());
    }

    scrollToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    toggleBodyClass(toggle, className) {
        if (toggle) {
            document.body.classList.add(className);
        } else {
            document.body.classList.remove(className);
        }
    }

};
