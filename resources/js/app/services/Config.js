import Log from './Log';

import config from './../../../../config';

export default new class {

    all() {
        return config;
    }

    get(key, silent = false) {
        let item = config[key];

        if (!config[key] && !silent) {
            Log.warning(`[Config] Cannot find "${key}".`);

            return;
        }

        return item;
    }

};
