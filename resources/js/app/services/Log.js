import Config from './Config';

export default new class {

    error(message, data = null, error = null) {
        if (!message && error) message = error.message;

        message = `[Log] [Error] ${message}`;

        if ('production' !== Config.get('environment')) window.console.log(message);

        //
    }

    info(message, data = null) {
        message = `[Log] [Info] ${message}`;

        if ('production' !== Config.get('environment')) window.console.log(message);

        //
    }

    warning(message, data = null) {
        message = `[Log] [Warning] ${message}`;

        if ('production' !== Config.get('environment')) window.console.log(message);

        //
    }

};
