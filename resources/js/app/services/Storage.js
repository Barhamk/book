import Log from './Log';

export default new class {

    constructor() {
        this.storage = window.localStorage;
    }

    clear() {
        this.storage.clear();
    }

    get(key, silent = false) {
        let item = this.storage.getItem(key);

        if (!item && !silent) {
            Log.warning(`[Storage] Cannot find or get "${key}".`);

            return;
        }

        return JSON.parse(item);
    }

    remove(key, silent = false) {
        let item = this.storage.getItem(key);

        if (!item && !silent) {
            Log.warning(`[Storage] Cannot find or remove "${key}".`);

            return;
        }

        this.storage.removeItem(key);
    }

    set(key, value) {
        this.storage.setItem(key, JSON.stringify(value));
    }

};
