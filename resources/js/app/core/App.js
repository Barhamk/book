import Axios from './Axios';
import Config from './../services/Config';
import FontAwesome from './FontAwesome';
import Middleware from './Middleware';
import Modal from './../components/Modal';
import PaneL from './../components/PaneL';
import PaneR from './../components/PaneR';
import PaneT from './../components/PaneT';
import Router from './Router';
import Storage from './../services/Storage';
import Store from './Store';
import Vue from 'vue';
import VuePortal from 'portal-vue';
import VueProgressBar from 'vue-progressbar';
import VueRouter from 'vue-router';

Vue.use(VuePortal);
Vue.use(VueProgressBar, Config.get('progressBar'));
Vue.use(VueRouter);

export default new Vue({

    components: {
        Modal,
        PaneL,
        PaneR,
        PaneT,
    },

    el: '#app',

    router: Router,

    store: Store,

    beforeCreate() {
        this.$store.commit('init');
    },

    created() {
        Axios.configure(this.$Progress);

        FontAwesome.configure();

        Middleware.configure(Router, this.$Progress);

        this.configureStores();

        this.configureVue();
    },

    methods: {
        configureStores() {
            Store.subscribe((mutation, state) => Storage.set('storeState', state));
        },

        configureVue() {
            Vue.config.devtools = ('development' === Config.get('environment'));

            Vue.config.productionTip = false;
        },
    },

});
