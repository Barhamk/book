import AppStore from './../stores/AppStore';
import AuthStore from './../stores/AuthStore';
import Config from './../services/Config';
import Storage from './../services/Storage';
import UserStore from './../stores/UserStore';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({

    strict: Config.get('environment') !== 'production',

    modules: {
        AppStore,
        AuthStore,
        UserStore,
    },

    mutations: {
        init(state) {
            const storeState = Storage.get('storeState', true);

            if (storeState) this.replaceState(Object.assign(state, storeState));
        }
    },

});
