import Helpers from './../services/Helpers';
import Storage from './../services/Storage';

export default new class {

    configure(Router, Progress) {
        Router.beforeEach((to, from, next) => {
            Progress.start();

            next();
        });

        Router.afterEach((to, from) => {
            Progress.finish();

            Helpers.metaTitle(to);
        });
    }

    isAuthenticated(to, from, next) {
        const storeState = Storage.get('storeState', true);

        if (!storeState || !storeState.AuthStore.authenticated) return next({name: '401', query: to.query});

        next();
    }

    isNotAuthenticated(to, from, next) {
        const storeState = Storage.get('storeState', true);

        if (storeState && storeState.AuthStore.authenticated) return next({name: 'auth-dashboard', query: to.query});

        next();
    }

};
