import Axios from 'axios';
import Store from './Store';

export default new class {

    configure(Progress) {
        const token = Store.getters['AuthStore/getToken']();

        if (token) this.configureAuthHeader(token);

        Axios.defaults.baseURL = '/api';

        Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        Axios.interceptors.request.use((response) => {
            Progress.start();

            this.submitButtonsDisabled();

            return response;
        }, (error) => {
            return Promise.reject(error);
        });

        Axios.interceptors.response.use((response) => {
            Progress.finish();

            this.configureAuthHeader(response.headers['x-auth-token']);

            this.submitButtonsEnabled();

            return response;
        }, (error) => {
            Progress.finish();

            this.submitButtonsEnabled();

            const code = error.response.status;

            if (code === 401) Store.dispatch('AuthStore/doLogout');

            return Promise.reject(error);
        });
    }

    configureAuthHeader(token) {
        if (token) {
            Store.commit('AuthStore/setToken', token);

            Axios.defaults.headers.common.Authorization = `Bearer ${token}`;
        }
    }

    submitButtonsDisabled() {
        let buttons = document.querySelector('button[type="submit"]');

        if (buttons) buttons.disabled = true;
    }

    submitButtonsEnabled() {
        let buttons = document.querySelector('button[type="submit"]');

        if (buttons) buttons.disabled = false;
    }

};
