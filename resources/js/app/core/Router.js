import Middleware from './Middleware';
import VueRouter from 'vue-router';

const AboutMeetTheTeam = () => import('./../views/about/MeetTheTeam');
const AboutOverview = () => import('./../views/about/Overview');
const AuthAccount = () => import('./../views/auth/Account');
const AuthDashboard = () => import('./../views/auth/Dashboard');
const AuthSettings = () => import('./../views/auth/Settings');
const AuthUsers = () => import('./../views/auth/Users');
const Error401 = () => import('./../views/Error401');
const Error404 = () => import('./../views/Error404');
const Home = () => import('./../views/Home');

export default new VueRouter({

    mode: 'history',

    routes: [
        {
            beforeEnter: Middleware.isAuthenticated,
            path: '/auth/account',
            name: 'auth-account',
            component: AuthAccount,
            meta: {title: 'Account'}
        },
        {
            beforeEnter: Middleware.isAuthenticated,
            path: '/auth/dashboard',
            name: 'auth-dashboard',
            component: AuthDashboard,
            meta: {title: 'Dashboard'}
        },
        {
            beforeEnter: Middleware.isAuthenticated,
            path: '/auth/settings',
            name: 'auth-settings',
            component: AuthSettings,
            meta: {title: 'Settings'}
        },
        {
            beforeEnter: Middleware.isAuthenticated,
            path: '/auth/users',
            name: 'auth-users',
            component: AuthUsers,
            meta: {title: 'Users'}
        },
        {
            beforeEnter: Middleware.isNotAuthenticated,
            path: '/',
            name: 'home',
            component: Home,
            meta: {title: 'Home'}
        },
        {
            beforeEnter: Middleware.isNotAuthenticated,
            path: '/about',
            name: 'about',
            component: AboutOverview,
            meta: {title: 'About'}
        },
        {
            beforeEnter: Middleware.isNotAuthenticated,
            path: '/about/meet-the-team',
            name: 'about-meet-the-team',
            component: AboutMeetTheTeam,
            meta: {title: 'Meet The Team'}
        },
        {
            path: '/401',
            name: '401',
            component: Error401,
            meta: {title: 'Error 401'}
        },
        {
            path: '*',
            name: '404',
            component: Error404,
            meta: {title: 'Error 404'}
        },
    ],

});
