import {library as FontAwesomeLibrary} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import Vue from 'vue';

import {
    faUserCircle as faUserCircleRegular,
} from '@fortawesome/free-regular-svg-icons';

import {
    faAlignLeft as faAlignLeftSolid,
    faChevronDown as faChevronDownSolid,
    faChevronLeft as faChevronLeftSolid,
    faChevronRight as faChevronRightSolid,
    faTimes as faTimesSolid,
} from '@fortawesome/free-solid-svg-icons';

export default new class {

    configure() {
        FontAwesomeLibrary.add([
            faAlignLeftSolid,
            faChevronDownSolid,
            faChevronLeftSolid,
            faChevronRightSolid,
            faTimesSolid,
            faUserCircleRegular,
        ]);

        Vue.component('font-awesome-icon', FontAwesomeIcon);
    }

};
