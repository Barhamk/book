<?php

use Illuminate\Database\Seeder;

class UserCustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userType = App\UserType::where('name', 'customer')->firstOrFail();

        factory(App\User::class, 100)->create([
            'user_type_id' => $userType->id,
        ])->each(static function ($user) {
            $user->profile()->save(factory(App\Profile::class)->make());
        });
    }
}
