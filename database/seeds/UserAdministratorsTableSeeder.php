<?php

use Illuminate\Database\Seeder;

class UserAdministratorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userType = App\UserType::where('name', 'administrator')->firstOrFail();

        factory(App\User::class)->create([
            'user_type_id' => $userType->id,
            'email' => 'im.darren.parker@icloud.com',
            'password' => password_hash('!Localhost1988', PASSWORD_DEFAULT),
        ])->each(static function ($user) {
            $user->profile()->save(factory(App\Profile::class)->make([
                'name' => 'Darren Parker'
            ]));
        });
    }
}
