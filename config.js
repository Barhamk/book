export default {

    'environment': process.env.NODE_ENV,
    'progressBar': {
        color: '#000000',
        failedColor: '#FF0000',
        thickness: '2px',
    },

};
