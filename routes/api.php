<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(static function () {
    Route::prefix('auth')->group(static function () {
        Route::post('/email-verify', 'AuthController@emailVerify');
        Route::post('/login', 'AuthController@login');
        Route::post('/password-reset', 'AuthController@passwordReset');
        Route::post('/password-reset-request', 'AuthController@passwordResetRequest');
        Route::post('/register', 'AuthController@register');

        Route::middleware('auth:api')->group(static function () {
            Route::post('/logout', 'AuthController@logout');
            Route::get('/me', 'AuthController@me');
            Route::post('/refresh', 'AuthController@refresh');
        });
    });

    Route::apiResource('users', 'UserController')->middleware('auth:api');
});
