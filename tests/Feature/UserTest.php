<?php

namespace Tests\Feature;

use App\Mail\UserCreatedMail;
use App\Repositories\EmailVerificationRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserTypeRepository;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    private function createAdministrator(): User
    {
        $userTypeRepository = resolve(UserTypeRepository::class);

        $userType = $userTypeRepository->findFirstWhere('name', 'administrator');

        return factory(User::class)->create([
            'user_type_id' => $userType->id,
        ]);
    }

    private function getAdministrator(User $userOld = null): User
    {
        $userRepository = resolve(UserRepository::class);

        $user = $userRepository->allAdministrators()->random(1)->first();

        if ($userOld && $userOld->is($user)) {
            return $this->getAdministrator($userOld);
        }

        return $user;
    }

    private function getCustomer(User $userOld = null): User
    {
        $userRepository = resolve(UserRepository::class);

        $user = $userRepository->allCustomers()->random(1)->first();

        if ($userOld && $userOld->is($user)) {
            return $this->getCustomer($userOld);
        }

        return $user;
    }

    public function testDestroySelf(): void
    {
        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customer))
            ->json('DELETE', '/api/users/' . $customer->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $customer->email);

        $this->assertNull($check);
    }

    public function testDestroyAdministratorAsAdministrator(): void
    {
        $administratorOne = $this->getAdministrator();

        $administratorTwo = $this->createAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administratorOne))
            ->json('DELETE', '/api/users/' . $administratorTwo->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $administratorTwo->email);

        $this->assertNull($check);
    }

    public function testDestroyCustomerAsAdministrator(): void
    {
        $administrator = $this->getAdministrator();

        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('DELETE', '/api/users/' . $customer->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $customer->email);

        $this->assertNull($check);
    }

    public function testDestroyReturnsUnauthorisedAsCustomerDestroyingUser(): void
    {
        $customerOne = $this->getCustomer();

        $customerTwo = $this->getCustomer($customerOne);

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customerOne))
            ->json('DELETE', '/api/users/' . $customerTwo->id);

        $response
            ->assertStatus(401)
            ->assertJson([
                'body' => 'Unauthorised'
            ]);
    }

    public function testDestroyReturnsUserNotFound(): void
    {
        $administrator = $this->getAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('DELETE', '/api/users/0');

        $response
            ->assertStatus(400)
            ->assertJson([
                'body' => 'User not found'
            ]);
    }

    public function testIndexAsAdministrator(): void
    {
        $administrator = $this->getAdministrator();

        $userRepository = resolve(UserRepository::class);

        $user = $userRepository->all()->first();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('GET', '/api/users');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $user->email
            ]);
    }

    public function testIndexReturnsUnauthorisedAsCustomer(): void
    {
        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customer))
            ->json('GET', '/api/users');

        $response
            ->assertStatus(401)
            ->assertJson([
                'body' => 'Unauthorised'
            ]);
    }

    public function testIndexWithoutPerPageFilter(): void
    {
        $administrator = $this->getAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('GET', '/api/users');

        $response
            ->assertStatus(200)
            ->assertJsonCount(10, 'body.data');
    }

    public function testIndexWithPerPageFilter(): void
    {
        $administrator = $this->getAdministrator();

        $perPage = 20;

        $data = [
            'perPage' => $perPage,
        ];

        $query = http_build_query($data);

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('GET', '/api/users?' . $query);

        $response
            ->assertStatus(200)
            ->assertJsonCount($perPage, 'body.data');
    }

    public function testShowAdministratorAsAdministrator(): void
    {
        $administratorOne = $this->getAdministrator();

        $administratorTwo = $this->createAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administratorOne))
            ->json('GET', '/api/users/' . $administratorTwo->id);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $administratorTwo->email
            ]);
    }

    public function testShowCustomerAsAdministrator(): void
    {
        $administrator = $this->getAdministrator();

        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('GET', '/api/users/' . $customer->id);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $customer->email
            ]);
    }

    public function testShowReturnsUnauthorisedAsCustomer(): void
    {
        $customerOne = $this->getCustomer();

        $customerTwo = $this->getCustomer($customerOne);

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customerOne))
            ->json('GET', '/api/users/' . $customerTwo->id);

        $response
            ->assertStatus(401)
            ->assertJson([
                'body' => 'Unauthorised'
            ]);
    }

    public function testShowReturnsUserNotFound(): void
    {
        $administrator = $this->getAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('GET', '/api/users/0');

        $response
            ->assertStatus(400)
            ->assertJson([
                'body' => 'User not found'
            ]);
    }

    public function testStoreWithInvalidCredentials(): void
    {
        $administrator = $this->getAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('POST', '/api/users');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
                'passwordConfirmation' => ['The password confirmation field is required.'],
            ]);
    }

    public function testStoreWithPasswordMismatch(): void
    {
        $administrator = $this->getAdministrator();

        $data = [
            'password' => 'password',
            'passwordConfirmation' => 'passwordFoo',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('POST', '/api/users', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'email' => ['The email field is required.'],
                'password' => ['The password and password confirmation must match.'],
            ]);
    }

    public function testStoreWithNotAUniqueEmail(): void
    {
        $administrator = $this->getAdministrator();

        $customer = $this->getCustomer();

        $data = [
            'name' => 'Foo Bar',
            'email' => $customer->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('POST', '/api/users', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email has already been taken.'],
            ]);
    }

    public function testStoreAsAdministrator(): void
    {
        Mail::fake();

        $administrator = $this->getAdministrator();

        $data = [
            'name' => 'Foo Bar',
            'email' => 'foo@bar.com',
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('POST', '/api/users', $data);

        $userRepository = resolve(UserRepository::class);

        $user = $userRepository->findFirstWhere('email', $data['email']);

        $this->assertEquals($data['email'], $user->email);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $user->email
            ]);

        Mail::assertSent(UserCreatedMail::class, static function ($mail) use ($user) {
            $emailVerificationRepository = resolve(EmailVerificationRepository::class);

            $emailVerification = $emailVerificationRepository->findFirstWhere('email', $user->email);

            if (null === $emailVerification) {
                return false;
            }

            $link = config('app.url') . '?modal=verify-email&email=' . $user->email . '&token=' . $emailVerification->token;

            $userCheck = $mail->user->id === $user->id;
            $linkCheck = $mail->link === $link;

            return $userCheck && $linkCheck;
        });
    }

    public function testStoreReturnsUnauthorisedAsCustomer(): void
    {
        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customer))
            ->json('POST', '/api/users');

        $response
            ->assertStatus(401)
            ->assertJson([
                'body' => 'Unauthorised'
            ]);
    }

    public function testUpdateSelfWithInvalidCredentials(): void
    {
        $customer = $this->getCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customer))
            ->json('PATCH', '/api/users/' . $customer->id);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
            ]);
    }

    public function testUpdateSelfWithAnExistingEmail(): void
    {
        $customerOne = $this->getCustomer();

        $customerTwo = $this->getCustomer($customerOne);

        $data = [
            'email' => $customerTwo->email,
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customerOne))
            ->json('PATCH', '/api/users/' . $customerOne->id, $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email has already been taken.'],
            ]);
    }

    public function testUpdateSelf(): void
    {
        $customer = $this->getCustomer();

        $data = [
            'email' => 'foo@bar.com',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customer))
            ->json('PATCH', '/api/users/' . $customer->id, $data);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $data['email'],
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $data['email']);

        $this->assertNotNull($check);
    }

    public function testUpdateAdministratorAsAdministrator(): void
    {
        $administratorOne = $this->getAdministrator();

        $administratorTwo = $this->createAdministrator();

        $data = [
            'email' => 'foo@bar.com',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administratorOne))
            ->json('PATCH', '/api/users/' . $administratorTwo->id, $data);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $data['email'],
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $data['email']);

        $this->assertNotNull($check);
    }

    public function testUpdateCustomerAsAdministrator(): void
    {
        $administrator = $this->getAdministrator();

        $customer = $this->getCustomer();

        $data = [
            'email' => 'foo@bar.com',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('PATCH', '/api/users/' . $customer->id, $data);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $data['email'],
            ]);

        $userRepository = resolve(UserRepository::class);

        $check = $userRepository->findFirstWhere('email', $data['email']);

        $this->assertNotNull($check);
    }

    public function testUpdateReturnsUnauthorisedAsCustomerUpdatingAnotherUser(): void
    {
        $customerOne = $this->getCustomer();

        $customerTwo = $this->getCustomer($customerOne);

        $data = [
            'email' => 'foo@bar.com',
        ];

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($customerOne))
            ->json('PATCH', '/api/users/' . $customerTwo->id, $data);

        $response
            ->assertStatus(401)
            ->assertJson([
                'body' => 'Unauthorised'
            ]);
    }

    public function testUpdateReturnsUserNotFound(): void
    {
        $administrator = $this->getAdministrator();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($administrator))
            ->json('PATCH', '/api/users/0');

        $response
            ->assertStatus(400)
            ->assertJson([
                'body' => 'User not found'
            ]);
    }
}
