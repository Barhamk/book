<?php

namespace Tests\Feature;

use App\EmailVerification;
use App\Mail\UserRegisteredMail;
use App\Mail\UserResetPasswordMail;
use App\Mail\UserResetPasswordRequestMail;
use App\PasswordReset;
use App\Repositories\EmailVerificationRepository;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserTypeRepository;
use App\Services\EmailVerificationService;
use App\Services\PasswordResetService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    private function createCustomer(): User
    {
        $userTypeRepository = resolve(UserTypeRepository::class);

        $userType = $userTypeRepository->findFirstWhere('name', 'customer');

        return factory(User::class)->create([
            'user_type_id' => $userType->id,
        ]);
    }

    private function createEmailVerification(User $user): EmailVerification
    {
        $emailVerificationService = resolve(EmailVerificationService::class);

        return $emailVerificationService->createNewEmailVerification($user);
    }

    private function createPasswordReset(User $user): PasswordReset
    {
        $passwordResetService = resolve(PasswordResetService::class);

        return $passwordResetService->createNewPasswordReset($user);
    }

    public function testEmailVerifyWithInvalidCredentials(): void
    {
        $response = $this->json('POST', '/api/auth/email-verify');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
                'token' => ['The token field is required.'],
            ]);
    }

    public function testEmailVerifyWithNotAnExistingEmail(): void
    {
        $data = [
            'email' => 'foo@bar.com',
            'token' => 'foo',
        ];

        $response = $this->json('POST', '/api/auth/email-verify', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The selected email is invalid.'],
            ]);
    }

    public function testEmailVerifyWithTokenMismatch(): void
    {
        $user = $this->createCustomer();

        $this->createEmailVerification($user);

        $data = [
            'email' => $user->email,
            'token' => 'foo',
        ];

        $response = $this->json('POST', '/api/auth/email-verify', $data);

        $response
            ->assertStatus(422)
            ->assertJson([
                'body' => 'Token Mismatch',
            ]);
    }

    public function testEmailVerify(): void
    {
        $user = $this->createCustomer();

        $emailVerification = $this->createEmailVerification($user);

        $data = [
            'email' => $user->email,
            'token' => $emailVerification->token,
        ];

        $response = $this->json('POST', '/api/auth/email-verify', $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $emailVerificationRepository = resolve(EmailVerificationRepository::class);

        $check = $emailVerificationRepository->findFirstWhere('email', $user->email);

        $this->assertNull($check);
    }

    public function testLoginWithInvalidCredentials(): void
    {
        $response = $this->json('POST', '/api/auth/login');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
            ]);
    }

    public function testLoginWithInvalidPasswordCredentials(): void
    {
        $user = $this->createCustomer();

        $data = [
            'email' => $user->email,
            'password' => 'foo',
        ];

        $response = $this->json('POST', '/api/auth/login', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'password' => ['Wrong password. Please try again.'],
            ]);
    }

    public function testLogin(): void
    {
        $user = $this->createCustomer();

        $data = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $response = $this->json('POST', '/api/auth/login', $data);

        $response
            ->assertStatus(200)
            ->assertHeader('x-auth-token');
    }

    public function testLogoutIsSuccess(): void
    {
        $user = $this->createCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($user))
            ->json('POST', '/api/auth/logout');

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);
    }

    public function testMe(): void
    {
        $user = $this->createCustomer();

        $response = $this
            ->withHeader('Authorization', 'Bearer ' . $this->getTokenFromUser($user))
            ->json('GET', '/api/auth/me');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'email' => $user->email,
            ]);
    }

    public function testPasswordResetRequestWithInvalidCredentials(): void
    {
        $response = $this->json('POST', '/api/auth/password-reset-request');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
            ]);
    }

    public function testPasswordResetRequestWithNotAnExistingEmail(): void
    {
        $data = [
            'email' => 'foo@bar.com',
        ];

        $response = $this->json('POST', '/api/auth/password-reset-request', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The selected email is invalid.'],
            ]);
    }

    public function testPasswordResetRequest(): void
    {
        Mail::fake();

        $user = $this->createCustomer();

        $data = [
            'email' => $user->email,
        ];

        $response = $this->json('POST', '/api/auth/password-reset-request', $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $passwordResetRepository = resolve(PasswordResetRepository::class);

        $check = $passwordResetRepository->findFirstWhere('email', $user->email);

        $this->assertNotNull($check);

        Mail::assertSent(UserResetPasswordRequestMail::class, static function ($mail) use ($user) {
            $passwordResetRepository = resolve(PasswordResetRepository::class);

            $passwordReset = $passwordResetRepository->findFirstWhere('email', $user->email);

            $link = config('app.url') . '?modal=reset-password&token=' . $passwordReset->token;

            $userCheck = $mail->user->id === $user->id;
            $linkCheck = $mail->link === $link;

            return $userCheck && $linkCheck;
        });
    }

    public function testPasswordResetWithInvalidCredentials(): void
    {
        $response = $this->json('POST', '/api/auth/password-reset');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
                'passwordConfirmation' => ['The password confirmation field is required.'],
            ]);
    }

    public function testPasswordResetWithInvalidCredentialsAndPasswordMismatch(): void
    {
        $data = [
            'password' => 'password',
            'passwordConfirmation' => 'passwordFoo',
        ];

        $response = $this->json('POST', '/api/auth/password-reset', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email field is required.'],
                'password' => ['The password and password confirmation must match.'],
            ]);
    }

    public function testPasswordResetWithNotAnExistingEmail(): void
    {
        $data = [
            'email' => 'foo@bar.com',
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this->json('POST', '/api/auth/password-reset', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The selected email is invalid.'],
            ]);
    }

    public function testPasswordResetWithNoToken(): void
    {
        $userExisting = $this->createCustomer();

        $data = [
            'email' => $userExisting->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this->json('POST', '/api/auth/password-reset', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['Please request a new password reset.'],
            ]);
    }

    public function testPasswordResetWithTokenMismatch(): void
    {
        $user = $this->createCustomer();

        $this->createPasswordReset($user);

        $data = [
            'email' => $user->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
            'token' => 'foo',
        ];

        $response = $this->json('POST', '/api/auth/password-reset', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['Please request a new password reset.'],
            ]);
    }

    public function testPasswordReset(): void
    {
        Mail::fake();

        $user = $this->createCustomer();

        $passwordReset = $this->createPasswordReset($user);

        $data = [
            'email' => $user->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
            'token' => $passwordReset->token,
        ];

        $response = $this->json('POST', '/api/auth/password-reset', $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'body' => 'Success',
            ]);

        $passwordResetRepository = resolve(PasswordResetRepository::class);

        $check = $passwordResetRepository->findFirstWhere('email', $user->email);

        $this->assertNull($check);

        Mail::assertSent(UserResetPasswordMail::class, static function ($mail) use ($user) {
            return $mail->user->id === $user->id;
        });
    }

    public function testRegisterWithInvalidCredentials(): void
    {
        $response = $this->json('POST', '/api/auth/register');

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
                'passwordConfirmation' => ['The password confirmation field is required.'],
            ]);
    }

    public function testRegisterWithPasswordMismatch(): void
    {
        $data = [
            'password' => 'password',
            'passwordConfirmation' => 'passwordFoo',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'name' => ['The name field is required.'],
                'email' => ['The email field is required.'],
                'password' => ['The password and password confirmation must match.'],
            ]);
    }

    public function testRegisterWithNotAUniqueEmail(): void
    {
        $customer = $this->createCustomer();

        $data = [
            'name' => 'Foo Bar',
            'email' => $customer->email,
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'email' => ['The email has already been taken.'],
            ]);
    }

    public function testRegister(): void
    {
        Mail::fake();

        $data = [
            'name' => 'Foo Bar',
            'email' => 'foo@bar.com',
            'password' => 'password',
            'passwordConfirmation' => 'password',
        ];

        $response = $this->json('POST', '/api/auth/register', $data);

        $response
            ->assertStatus(200)
            ->assertHeader('x-auth-token');

        $userRepository = resolve(UserRepository::class);

        $user = $userRepository->findFirstWhere('email', $data['email']);

        $this->assertEquals($data['email'], $user->email);

        Mail::assertSent(UserRegisteredMail::class, static function ($mail) use ($user) {
            $emailVerificationRepository = resolve(EmailVerificationRepository::class);

            $emailVerification = $emailVerificationRepository->findFirstWhere('email', $user->email);

            $link = config('app.url') . '?modal=verify-email&email=' . $user->email . '&token=' . $emailVerification->token;

            $userCheck = $mail->user->id === $user->id;
            $linkCheck = $mail->link === $link;

            return $userCheck && $linkCheck;
        });
    }

    public function testUnauthenticated(): void
    {
        $response = $this->json('GET', '/api/auth/me');

        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated',
            ]);
    }
}
