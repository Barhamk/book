<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getTokenFromUser(User $user): string
    {
        return JWTAuth::fromUser($user);
    }
}
